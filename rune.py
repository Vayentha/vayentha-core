
import os
import sys
import requests


class GreaterGeneralRune:
    def __init__(self, n):
        self.name = n
        self.linkedRunes = []
        self.desc = ""

    def add_desc(self, d):
        self.desc = d
        print(f"added {d} to general rune.")
        print(self.desc)

    # point to Vayentha Gitlab to check for other projects we may want to add
    def addLinkedRunes(self, r):
        if r is None:
            return
        try:
            resp = self.checkGitlab(r)
            if resp == 200:
                self.linkedRunes.append(r)
            if resp == 404:
                print("error! project not found. Did you tpe the name correctly?")
                return
        except requests.ConnectionError:
            print("failed to connect!")
            return

    def checkGitlab(self, runes):
        http = f'https://gitlab.com/Vayentha/{runes}'
        req = requests.head(http)
        # print(req.status_code) # SHOULD BE 200
        return req.status_code

    def checkGitlabConfigs(self, debug=None):
        for rune in self.linkedRunes:
            try:
                if debug == True:
                    print(rune)
                self.checkGitlab(rune)
            except Exception as e:
                print(e)
                return
        return

r = GreaterGeneralRune("testingrunes")
r.add_desc("testing the description adding mechanism")
r.addLinkedRunes('bag_end')
# r.addLinkedRunes('thiswillnotwork')
r.checkGitlabConfigs()